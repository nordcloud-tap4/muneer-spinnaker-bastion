variable "project" {
  description = "The ID of the project to apply any resources to."
#  default = "trump-spin-5"
}

variable "default_region" {
  description = "Default region"
  default     = "europe-north1"
}

variable "default_zone" {
  description = "Default zone"
  default     = "europe-north1-a"
}

variable "service_account" {
  description = "Service account for bastion"
  default     = "bastion-compute"
}
