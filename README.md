# Install VM with ServiceAccount to use for running terraform

### Prerequisite 

* None 

### Variables
* `project`: The ID of the GCP project

## Installation steps
terraform init
terraform plan -out=terraform.plan
terraform apply terraform.plan
