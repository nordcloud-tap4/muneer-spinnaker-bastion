variable "project" {
  description = "The ID of the project to apply any resources to."
}

variable "region" {
  description = "Default region"
}

variable "zone" {
  description = "Default zone"
}

variable "depends_on" {
  default     = []
  type        = "list"
  description = "Hack for expressing module to module dependency"
}

variable "vpc" {
  description = "Default VPC"
  default     = "default"
}

variable "service_account" {
  description = "Service account for bastion"
}

variable "interpreter" {
  description = "Intepreter to run command"
  default     = ["bash", "-c"]
# default     = ["PowerShell", "-Command"]
}
