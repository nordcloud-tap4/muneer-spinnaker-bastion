resource "google_service_account" "bastion_service_account" {
  account_id   = "${var.service_account}"
  display_name = "${var.service_account}"
}

resource "google_service_account_iam_binding" "bastion_service_account_compute_admin" {
  service_account_id = "${google_service_account.bastion_service_account.name}"
  role               = "roles/compute.admin"
  members            = [
    "serviceAccount:${google_service_account.bastion_service_account.email}"
  ]
}

resource "google_service_account_iam_binding" "bastion_service_account_service_account_user" {
  service_account_id = "${google_service_account.bastion_service_account.name}"
  role               = "roles/iam.serviceAccountUser"
  members            = [
    "serviceAccount:${google_service_account.bastion_service_account.email}"
  ]
}

resource "google_service_account_iam_binding" "bastion_service_account_service_account_creator" {
  service_account_id = "${google_service_account.bastion_service_account.name}"
  role               = "roles/iam.serviceAccountCreator"
  members            = [
    "serviceAccount:${google_service_account.bastion_service_account.email}"
  ]
}

resource "google_service_account_iam_binding" "bastion_service_account_service_usage_admin" {
  service_account_id = "${google_service_account.bastion_service_account.name}"
  role               = "roles/serviceusage.serviceUsageAdmin"
  members            = [
    "serviceAccount:${google_service_account.bastion_service_account.email}"
  ]
}

resource "google_service_account_iam_binding" "bastion_service_account_project_iam_admin" {
  service_account_id = "${google_service_account.bastion_service_account.name}"
  role               = "roles/iam.projectIamAdmin"
  members            = [
    "serviceAccount:${google_service_account.bastion_service_account.email}"
  ]
}

resource "google_service_account_iam_binding" "bastion_service_account_service_account_key_admin" {
  service_account_id = "${google_service_account.bastion_service_account.name}"
  role               = "roles/iam.serviceAccountKeyAdmin"
  members            = [
    "serviceAccount:${google_service_account.bastion_service_account.email}"
  ]
}

resource "google_service_account_iam_binding" "bastion_service_account_pubsub_admin" {
  service_account_id = "${google_service_account.bastion_service_account.name}"
  role               = "roles/pubsub.admin"
  members            = [
    "serviceAccount:${google_service_account.bastion_service_account.email}"
  ]
}

resource "google_service_account_iam_binding" "bastion_service_account_container_cluster_admin" {
  service_account_id = "${google_service_account.bastion_service_account.name}"
  role               = "roles/container.clusterAdmin"
  members            = [
    "serviceAccount:${google_service_account.bastion_service_account.email}"
  ]
}

resource "google_service_account_iam_binding" "bastion_service_account_container_admin" {
  service_account_id = "${google_service_account.bastion_service_account.name}"
  role               = "roles/container.admin"
  members            = [
    "serviceAccount:${google_service_account.bastion_service_account.email}"
  ]
}

resource "google_service_account_iam_binding" "bastion_service_account_storage_admin" {
  service_account_id = "${google_service_account.bastion_service_account.name}"
  role               = "roles/storage.admin"
  members            = [
    "serviceAccount:${google_service_account.bastion_service_account.email}"
  ]
}
/*
# doing like this as google_service_account_iam_binding is failing for some roles
data "template_file" "bastion_service_account_roles" {
  template = <<EOF
gcloud projects add-iam-policy-binding $${project} \
 --member serviceAccount:$${sa_email} \
 --role roles/compute.admin
gcloud projects add-iam-policy-binding $${project} \
 --member serviceAccount:$${sa_email} \
 --role roles/iam.projectIamAdmin
gcloud projects add-iam-policy-binding $${project} \
 --member serviceAccount:$${sa_email} \
 --role roles/pubsub.admin
gcloud projects add-iam-policy-binding $${project} \
 --member serviceAccount:$${sa_email} \
 --role roles/container.clusterAdmin
gcloud projects add-iam-policy-binding $${project} \
 --member serviceAccount:$${sa_email} \
 --role roles/container.admin
 gcloud projects add-iam-policy-binding $${project} \
 --member serviceAccount:$${sa_email} \
 --role roles/storage.admin
EOF

  vars {
    project  = "${var.project}"
    sa_email = "${google_service_account.bastion_service_account.email}"
  }
}

resource "null_resource" "bastion_service_account_roles" {
  provisioner "local-exec" {
    command = "${data.template_file.bastion_service_account_roles.rendered}"
    interpreter = "${var.interpreter}"
  }
}
*/
