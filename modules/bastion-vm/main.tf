data "template_file" "bastion_vm_init" {
  template = <<EOF
export CLOUD_SDK_REPO="cloud-sdk-$(lsb_release -c -s)"
echo "deb http://packages.cloud.google.com/apt $CLOUD_SDK_REPO main" | sudo tee -a /etc/apt/sources.list.d/google-cloud-sdk.list
curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
sudo apt-get update
sudo apt-get install google-cloud-sdk
sudo apt-get install unzip
sudo apt-get install wget
wget https://releases.hashicorp.com/terraform/0.11.13/terraform_0.11.13_linux_amd64.zip
unzip terraform_0.11.13_linux_amd64.zip
sudo mv terraform /usr/local/bin/
EOF
}

resource "google_compute_instance" "bastion" {
  name         = "${var.bastion_vm}"
  machine_type = "${var.bastion_vm_machine}"
  zone         = "${var.zone}"

  boot_disk {
    initialize_params {
      image = "${var.bastion_vm_image}"
    }
  }

  network_interface {
    network = "${var.vpc}"
    access_config {
      // Ephemeral IP
    }
  }

  metadata_startup_script = "${data.template_file.bastion_vm_init.rendered}"

  service_account {
    email  = "${var.service_account_email}"
    scopes = ["cloud-platform"]
  }
}
