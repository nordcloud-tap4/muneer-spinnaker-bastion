variable "project" {
  description = "The ID of the project to apply any resources to."
}

variable "region" {
  description = "Default region"
}

variable "zone" {
  description = "Default zone"
}

variable "depends_on" {
  default     = []
  type        = "list"
  description = "Hack for expressing module to module dependency"
}

variable "vpc" {
  description = "Default VPC"
  default     = "default"
}

variable "service_account_email" {
  description = "Service account for bastion"
}

variable "bastion_vm" {
  description = "Bastion VM Name"
  default     = "trump-bastion-1"
}

variable "bastion_vm_machine" {
  description = "Bastion VM machine type"
  default     = "g1-small"
}

variable "bastion_vm_image" {
  description = "Bastion VM image type"
  default     = "ubuntu-os-cloud/ubuntu-1804-bionic-v20190429"
}

variable "interpreter" {
  description = "Intepreter to run command"
  default     = ["bash", "-c"]
# default     = ["PowerShell", "-Command"]
}
