output "bastion_instance_id" {
  value = "${google_compute_instance.bastion.instance_id}"
}
