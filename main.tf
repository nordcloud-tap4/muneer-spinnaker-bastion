provider "google" {
  version = "~> 2.5.1"
  project = "${var.project}"
}

provider "google-beta" {
  version = "~> 2.5.1"
  project = "${var.project}"
}

# serviceusage.googleapis.com has to be enabled manually
resource "google_project_service" "project_service_cloudresource" {
  service = "cloudresourcemanager.googleapis.com"
  disable_on_destroy = false
}

resource "google_project_service" "project_service_iam" {
  service = "iam.googleapis.com"
  disable_on_destroy = false
}

resource "google_project_service" "project_service_iamcredentials" {
  service = "iamcredentials.googleapis.com"
  disable_on_destroy = false
}

resource "google_project_service" "project_service_compute" {
  service = "compute.googleapis.com"
  disable_on_destroy = false
}

resource "google_project_service" "project_service_container" {
  service = "container.googleapis.com"
  disable_on_destroy = false
}
resource "google_project_service" "project_service_containerregistry" {
  service = "containerregistry.googleapis.com"
  disable_on_destroy = false
}
resource "google_project_service" "project_service_pubsub" {
  service = "pubsub.googleapis.com"
  disable_on_destroy = false
}


module "bastion_service_account" {
  source = "modules/bastion-service-account"
  region = "${var.default_region}"
  zone   = "${var.default_zone}"
  service_account = "${var.service_account}"

  ## Project id is added as tag to create dependency between project service and this module
  project = "${var.project}"

  depends_on = [
    "google_project_service.project_service_cloudresource.id",
    "google_project_service.project_service_iam.id",
    "google_project_service.project_service_iamcredentials.id",
    "google_project_service.project_service_compute.id",
    "google_project_service.project_service_container.id",
    "google_project_service.project_service_containerregistry.id",
    "${google_project_service.project_service_pubsub.id}"
  ]
}

module "bastion_vm" {
  source = "modules/bastion-vm"
  region = "${var.default_region}"
  zone   = "${var.default_zone}"

  ## Project id is added as tag to create dependency between project service and this module
  project = "${var.project}"
  service_account_email = "${var.service_account}@${var.project}.iam.gserviceaccount.com"

  depends_on = [
    "modules.bastion_service_account"
  ]
}
